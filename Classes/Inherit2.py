class Mammal():
    def __init__(self, mammalName):
        print(mammalName, 'is a warm-blooded animal.')
        print('super=',super)


class Dog(Mammal):
    def __init__(self):
        print('Dog has four legs.')
        super().__init__('Dog')


d1 = Dog()