import pytest, xlrd



def get_data(file_name):
    #
    # create an empty list to store rows
    rows = []
    # open the specified Excel spreadsheet as workbook
    book = xlrd.open_workbook(file_name)
    # get the first sheet
    sheet = book.sheet_by_index(0)
    # iterate through the sheet and get data from rows in list
    for row_idx in range(1, sheet.nrows):
        #print(sheet.row_values(row_idx, 0, sheet.ncols))
        rows.append(list(sheet.row_values(row_idx, 0, sheet.ncols)))
    return rows


def fun():
    params = [
        # tuple with (input, expectedOutput)
        ['a100', 'b100'],
        ['aa', 'b1']
    ]

    return params


@pytest.fixture( params=fun())
def test_data(request):
    return request.param

def test_markdown(test_data):
    (the_input, the_expected_output) = get_data('Excel_Sample.xlsx')
    print(the_input)
    print(the_expected_output)

