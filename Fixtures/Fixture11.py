import pytest


def fun():
    params = [
        # tuple with (input, expectedOutput)
        ('a100', 'b100'),
        ('aa', 'b1')
    ]

    return params


@pytest.fixture( params=fun())
def test_data(request):
    return request.param

def test_markdown(test_data):
    (the_input, the_expected_output) = test_data
    print(the_input)
    print(the_expected_output)

