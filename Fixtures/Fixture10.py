import pytest


@pytest.fixture( params=[
    # tuple with (input, expectedOutput)
    ('a'   , 'b'),
    ('aa', 'b1')
    ])
def test_data(request):
    return request.param

def test_markdown(test_data):
    (the_input, the_expected_output) = test_data
    print(the_input)
    print(the_expected_output)

