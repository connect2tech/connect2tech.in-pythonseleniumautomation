import unittest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

driver = webdriver.Chrome('D:/nchaurasia/Python-Architect/connect2tech.in-PythonSeleniumAutomation/chromedriver_win32_2.46/chromedriver.exe')
driver.get('https://qatechnicals.wordpress.com/')

element1 = driver.find_element_by_xpath("//*[@id='menu-item-593']/a")
element2 = driver.find_element_by_xpath("//*[@id='menu-item-637']/a")
element3 = driver.find_element_by_xpath("//*[@id='menu-item-671']/a")

hoverover = ActionChains(driver).move_to_element(element1).\
    move_to_element(element2).\
    move_to_element(
    element3).click().\
    perform()

WebDriverWait(driver, 30).until(expected_conditions.title_contains("XML"))

print(driver.title)
