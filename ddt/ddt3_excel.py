import xlrd, unittest
from ddt import ddt, data, unpack
from selenium import webdriver

def get_data(file_name):
    #
    # create an empty list to store rows
    rows = []
    # open the specified Excel spreadsheet as workbook
    book = xlrd.open_workbook(file_name)
    # get the first sheet
    sheet = book.sheet_by_index(0)
    # iterate through the sheet and get data from rows in list
    for row_idx in range(1, sheet.nrows):
        #print(sheet.row_values(row_idx, 0, sheet.ncols))
        rows.append(list(sheet.row_values(row_idx, 0, sheet.ncols)))
    return rows

@ddt
class SearchExcelDDT(unittest.TestCase):


    def setUp(self):
        # create a new Firefox session
        #self.driver = webdriver.Firefox()
        #self.driver.implicitly_wait(30)
        #self.driver.maximize_window()
        # navigate to the application home page
        #self.driver.get("http://demo.magentocommerce.com/")
        print('setUp')

    # get the data from specified Excel spreadsheet
    # by calling the get_data function
    @data(*get_data("Excel_Sample.xlsx"))
    @unpack
    def test_search(self, search_value, expected_count):
        print('search_value=',search_value)
        print('expected_count=', expected_count)

if __name__ == '__main__':
    unittest.main()