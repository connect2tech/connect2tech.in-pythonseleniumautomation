import unittest
from ddt import ddt, data, idata, file_data, unpack

import unittest
from ddt import ddt, file_data


@ddt
class TestDDTDataFile(unittest.TestCase):

    @file_data('mydatafile.json')
    def test_with_ddt_file_data(self, x):
        self.assertGreater(x, 0)

if __name__ == '__main__':
    unittest.main()