import unittest
from ddt import ddt, data, idata, file_data, unpack


@ddt
class TestDDTData(unittest.TestCase):

    @data(1, -2, 3, 4, -5)
    def test_with_ddt_data(self, x):
        self.assertGreater(x, 0)
