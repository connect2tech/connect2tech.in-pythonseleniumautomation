import pytest


def test_zero_division():
    print('Inside...')
    with pytest.raises(ZeroDivisionError):
        1 / 0